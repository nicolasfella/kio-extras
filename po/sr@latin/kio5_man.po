# Translation of kio5_man.po into Serbian.
# Toplica Tanaskovic <toptan@kde.org.yu>, 2003.
# Chusslove Illich <caslav.ilic@gmx.net>, 2005, 2007, 2008, 2009, 2012, 2017.
# Dalibor Djuric <dalibor.djuric@mozilla-srbija.org>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kio5_man\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-04-06 03:18+0200\n"
"PO-Revision-Date: 2017-09-28 18:00+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: kio_man.cpp:480
#, kde-format
msgid ""
"No man page matching to %1 found.<br /><br />Check that you have not "
"mistyped the name of the page that you want.<br />Check that you have typed "
"the name using the correct upper and lower case characters.<br />If "
"everything looks correct, then you may need to improve the search path for "
"man pages; either using the environment variable MANPATH or using a matching "
"file in the /etc directory."
msgstr ""
"<html>Nije nađena nijedna uputna stranica koja odgovara %1.<br/><br/"
">Proverite da li ste ispravno uneli ime stranice koju želite.<br/>Ne "
"zaboravite da morate paziti na razliku velikih i malih slova.<br/>Ako sve "
"deluje tačno, možda morate zadati bolju putanju traženja uputnih stranica, "
"ili u promenljivoj okruženja <tt>$MANPATH</tt> ili u odgovarajućem fajlu u "
"fascikli <tt>/etc</tt>.</html>"

#: kio_man.cpp:511
#, kde-format
msgid "Open of %1 failed."
msgstr "Otvaranje %1 nije uspelo."

#: kio_man.cpp:581
#, kde-format
msgid ""
"The specified man page referenced another page '%1',<br />but the referenced "
"page '%2' could not be found."
msgstr ""

#: kio_man.cpp:617 kio_man.cpp:636
#, kde-format
msgid "Man output"
msgstr "Izlaz naredbe man"

#: kio_man.cpp:621
#, kde-format
msgid "<h1>KDE Man Viewer Error</h1>"
msgstr "<h1>Greška KDE‑ovog prikazivača uputnih stranica</h1>"

#: kio_man.cpp:640
#, kde-format
msgid "There is more than one matching man page."
msgstr "Više od jedne uputne stranice odgovara upitu."

#: kio_man.cpp:651
#, kde-format
msgid ""
"Note: if you read a man page in your language, be aware it can contain some "
"mistakes or be obsolete. In case of doubt, you should have a look at the "
"English version."
msgstr ""
"Napomena: Ako čitate uputnu stranicu na svom jeziku, imajte na umu da može "
"imati neke greške ili biti zastarela. Ako posumnjate, pogledajte i englesku "
"verziju."

#: kio_man.cpp:728
#, kde-format
msgid "Header files"
msgstr "Fajlovi zaglavlja"

#: kio_man.cpp:729
#, kde-format
msgid "Header files (POSIX)"
msgstr "Fajlovi zaglavlja (POSIX)"

#: kio_man.cpp:730
#, kde-format
msgid "User Commands"
msgstr "Korisničke naredbe"

#: kio_man.cpp:731
#, kde-format
msgid "User Commands (POSIX)"
msgstr "Korisničke naredbe (POSIX)"

#: kio_man.cpp:732
#, kde-format
msgid "System Calls"
msgstr "Sistemski pozivi"

#: kio_man.cpp:733
#, kde-format
msgid "Subroutines"
msgstr "Podrutine"

#: kio_man.cpp:734
#, kde-format
msgid "Perl Modules"
msgstr "Perl moduli"

#: kio_man.cpp:735
#, kde-format
msgid "Network Functions"
msgstr "Mrežne funkcije"

#: kio_man.cpp:736
#, kde-format
msgid "Devices"
msgstr "Uređaji"

#: kio_man.cpp:737
#, kde-format
msgid "File Formats"
msgstr "Formati fajlova"

#: kio_man.cpp:738
#, kde-format
msgid "Games"
msgstr "Igre"

#: kio_man.cpp:739
#, kde-format
msgid "Miscellaneous"
msgstr "Razno"

#: kio_man.cpp:740
#, kde-format
msgid "System Administration"
msgstr "Administracija sistema"

#: kio_man.cpp:741
#, kde-format
msgid "Kernel"
msgstr "Jezgro"

#: kio_man.cpp:742
#, kde-format
msgid "Local Documentation"
msgstr "Lokalna dokumentacija"

# >> @title Section of man pages, as the several above
#: kio_man.cpp:743
#, kde-format
msgid "New"
msgstr "Novo"

#: kio_man.cpp:778 kio_man.cpp:782 kio_man.cpp:1133
#, kde-format
msgid "UNIX Manual Index"
msgstr "Indeks Unix uputa"

#: kio_man.cpp:808
#, kde-format
msgid "Section %1"
msgstr "Odeljak %1"

#: kio_man.cpp:1142
#, kde-format
msgid "Index for Section %1: %2"
msgstr "Indeks za odeljak %1: %2"

#: kio_man.cpp:1147
#, kde-format
msgid "Generating Index"
msgstr "Stvaram indeks"

#: kio_man.cpp:1412
#, kde-format
msgid ""
"Could not find the sgml2roff program on your system. Please install it, if "
"necessary, and extend the search path by adjusting the environment variable "
"PATH before starting KDE."
msgstr ""
"Ne mogu da nađem naredbu sgml2roff. Instalirajte je ukoliko je potrebno, i "
"proširite putanju pretrage dopunjavanjem promenljive okruženja $PATH (pre "
"nego što se KDE pokrene)."
