# translation of kio_sftp.po to galician
# Copyright (C) 2002, 2003, 2006, 2007, 2008, 2009 Free Software Foundation, Inc.
# Javier Jardón <javierjc@mundo-r.com>, 2002, 2003.
# Xabi G. Feal <xabigf@gmx.net>, 2006.
# mvillarino <mvillarino@users.sourceforge.net>, 2007, 2008, 2009.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2012, 2013, 2014.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:32+0000\n"
"PO-Revision-Date: 2023-06-11 16:12+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: kio_sftp.cpp:228
#, kde-format
msgid "Incorrect or invalid passphrase"
msgstr "A frase de paso ou é incorrecta ou incorrecta"

#: kio_sftp.cpp:277
#, kde-format
msgid "Could not allocate callbacks"
msgstr "Non foi posíbel asignar memoria ás chamadas"

#: kio_sftp.cpp:290
#, kde-format
msgid "Could not set log verbosity."
msgstr "Non foi posíbel definir o nivel de detalle do rexistro."

#: kio_sftp.cpp:295
#, kde-format
msgid "Could not set log userdata."
msgstr "Non foi posíbel definir o rexistro dos datos do usuario."

#: kio_sftp.cpp:300
#, kde-format
msgid "Could not set log callback."
msgstr "Non foi posíbel definir a resposta do rexistro."

#: kio_sftp.cpp:336 kio_sftp.cpp:338 kio_sftp.cpp:854
#, kde-format
msgid "SFTP Login"
msgstr "Acceso por SFTP"

#: kio_sftp.cpp:353
#, kde-format
msgid "Use the username input field to answer this question."
msgstr ""
"Use o campo de entrada do nome de usuaria para responder esta pregunta."

#: kio_sftp.cpp:365
#, kde-format
msgid "Please enter your password."
msgstr "Insira o seu contrasinal."

#: kio_sftp.cpp:370 kio_sftp.cpp:857
#, kde-format
msgid "Site:"
msgstr "Sitio:"

#: kio_sftp.cpp:416
#, kde-format
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr "Non foi posíbel ler a ligazón: %1 [%2]"

#: kio_sftp.cpp:538
#, kde-format
msgid "Could not create a new SSH session."
msgstr "Non foi posíbel crear unha sesión de SSH nova."

#: kio_sftp.cpp:549 kio_sftp.cpp:553
#, kde-format
msgid "Could not set a timeout."
msgstr "Non foi posíbel definir o tempo de espera."

#: kio_sftp.cpp:560
#, kde-format
msgid "Could not disable Nagle's Algorithm."
msgstr "Non foi posíbel desactivar o algoritmo de Nagle."

#: kio_sftp.cpp:566 kio_sftp.cpp:571
#, kde-format
msgid "Could not set compression."
msgstr "Non foi posíbel definir a compresión."

#: kio_sftp.cpp:577
#, kde-format
msgid "Could not set host."
msgstr "Non foi posíbel definir o servidor."

#: kio_sftp.cpp:583
#, kde-format
msgid "Could not set port."
msgstr "Non foi posíbel definir o porto."

#: kio_sftp.cpp:591
#, kde-format
msgid "Could not set username."
msgstr "Non foi posíbel definir o nome de usuaria."

#: kio_sftp.cpp:598
#, kde-format
msgid "Could not parse the config file."
msgstr "Non foi posíbel procesar o ficheiro de configuración."

#: kio_sftp.cpp:615
#, kde-kuit-format
msgid "Opening SFTP connection to host %1:%2"
msgstr "Abrindo unha conexión SFTP co servidor %1:%2."

#: kio_sftp.cpp:655
#, kde-format
msgid "Could not get server public key type name"
msgstr "Non foi posíbel obter o nome do tipo de chave pública do servidor"

#: kio_sftp.cpp:667
#, kde-format
msgid "Could not create hash from server public key"
msgstr "Non foi posíbel crear o hash da chave pública do servidor"

#: kio_sftp.cpp:676
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""
"Non foi posíbel crear a pegada dixital para a chave pública do servidor"

#: kio_sftp.cpp:736
#, kde-format
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""
"Non se atopou unha chave %1 para este servidor pero existe outro tipo de "
"chave.\n"
"Un atacante podería cambiar a chave predeterminada do servidor para "
"confundir o seu cliente e facerlle pensa que a chave non existe.\n"
"Contacte co administrador do sistema.\n"
"%2"

#: kio_sftp.cpp:753
#, kde-format
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr "Cambio de identidade de servidor"

#: kio_sftp.cpp:755
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""
"<para>Cambiouse a chave do servidor <emphasis>%1</emphasis>.</"
"para><para>Isto pode indicar ou que se está facendo «spoofing» do DNS ou que "
"o enderezo IP do servidor e a súa chave cambiaron á vez.</para> <para>A "
"pegada dixital da chave %2 enviada polo servidor remoto é:<bcode>%3</"
"bcode>Seguro que quere continuar?</para>"

#: kio_sftp.cpp:765
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr "Fallo de verificación do servidor"

#: kio_sftp.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""
"<para>Non se pode estabelecer a autenticidade do servidor <emphasis>%1</"
"emphasis>.</para><para>A pegada dixital da chave %2 é:<bcode>%3</"
"bcode>Seguro que quere continuar conectando?</para>"

#: kio_sftp.cpp:776
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr "Conectar aínda así"

#: kio_sftp.cpp:799 kio_sftp.cpp:818 kio_sftp.cpp:833 kio_sftp.cpp:846
#: kio_sftp.cpp:898 kio_sftp.cpp:908
#, kde-format
msgid "Authentication failed."
msgstr "Fallou a autenticación."

#: kio_sftp.cpp:806
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""
"Fallou a autenticación. O servidor non enviou ningún método de autenticación."

#: kio_sftp.cpp:855
#, kde-format
msgid "Please enter your username and password."
msgstr "Insira o seu nome de usuaria e o contrasinal."

#: kio_sftp.cpp:866
#, kde-format
msgid "Incorrect username or password"
msgstr "O nome de usuaria ou o contrasinal é incorrecto"

#: kio_sftp.cpp:915
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""
"Non é posíbel pedir o subsistema SFTP. Asegúrese de que SFTP está activado "
"no servidor."

#: kio_sftp.cpp:920
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr "Non foi posíbel inicializar a sesión SFTP."

#: kio_sftp.cpp:924
#, kde-format
msgid "Successfully connected to %1"
msgstr "Conectou con %1"

#: kio_sftp.cpp:977
#, kde-format
msgid "Invalid sftp context"
msgstr "Contexto de SFTP incorrecto"

#: kio_sftp.cpp:1538
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""
"Non foi posíbel cambiar os permisos de\n"
"%1"

#~ msgid ""
#~ "The host key for the server %1 has changed.\n"
#~ "This could either mean that DNS SPOOFING is happening or the IP address "
#~ "for the host and its host key have changed at the same time.\n"
#~ "The fingerprint for the %2 key sent by the remote host is:\n"
#~ "  SHA256:%3\n"
#~ "Please contact your system administrator.\n"
#~ "%4"
#~ msgstr ""
#~ "Cambiouse a chave do servidor %1.\n"
#~ "Isto pode significar ou que se está producindo DNS SPOOFING ou que se "
#~ "cambiaron o enderezo IP do servidor e a súa chave á vez.\n"
#~ "A pegada dixital da chave %2 enviada polo servidor remoto é:\n"
#~ "  SHA256:%3\n"
#~ "Contacte co administrador do sistema.\n"
#~ "%4"

#~ msgid "Warning: Cannot verify host's identity."
#~ msgstr "Aviso: Non se pode verificar a identidade do servidor."

#~ msgid ""
#~ "The host key for this server was not found, but another type of key "
#~ "exists.\n"
#~ "An attacker might change the default server key to confuse your client "
#~ "into thinking the key does not exist.\n"
#~ "Please contact your system administrator.\n"
#~ "%1"
#~ msgstr ""
#~ "Non se atopou a chave para este servidor pero existe outro tipo de "
#~ "chave.\n"
#~ "Un atacante podería cambiar a chave predeterminada do servidor para "
#~ "confundir o seu cliente e facerlle pensa que a chave non existe.\n"
#~ "Contacte co administrador do sistema.\n"
#~ "%1"

#~ msgid ""
#~ "The authenticity of host %1 cannot be established.\n"
#~ "The key fingerprint is: %2\n"
#~ "Are you sure you want to continue connecting?"
#~ msgstr ""
#~ "Non se pode estabelecer a autenticidade do servidor %1.\n"
#~ "A pegada dixital da chave é: %2\n"
#~ "Seguro que quere continuar a conectar?"

#~ msgid "No hostname specified."
#~ msgstr "Non se especificou ningún servidor."
