# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jure Repinc <jlp@holodeck1.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:32+0000\n"
"PO-Revision-Date: 2012-06-27 14:25+0200\n"
"Last-Translator: Jure Repinc <jlp@holodeck1.com>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 1.5\n"

#: recentdocuments.cpp:151
#, kde-format
msgid "Recent Documents"
msgstr "Nedavni dokumenti"
